#!/bin/bash

if ! dpkg-statoverride --list "$1" >/dev/null; then
  if [ "$1" == "/etc/biomaj/db_properties/global.properties" ]; then
    dpkg-statoverride --update --add tomcat8 root 0660  $1
  else
    dpkg-statoverride --update --add tomcat8 root 0775  $1
  fi
fi
