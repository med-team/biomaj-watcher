# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the biomaj-watcher package.
#
# Yuri Kozlov <yuray@komyakino.ru>, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: biomaj-watcher 1.2.0-2\n"
"Report-Msgid-Bugs-To: biomaj-watcher@packages.debian.org\n"
"POT-Creation-Date: 2012-01-31 07:16+0100\n"
"PO-Revision-Date: 2012-02-01 20:31+0400\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: string
#. Description
#: ../templates:2001
msgid "Login for administration interface of BioMAJ:"
msgstr "Учётная запись для интерфейса управления BioMAJ:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Please enter the login to use when connecting to the web administration "
"interface of BioMAJ."
msgstr ""
"Введите имя учётной записи для входа в веб-интерфейс управления BioMAJ."

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Configure LDAP authentication?"
msgstr "Настроить аутентификацию LDAP?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"Please choose whether LDAP authentication for BioMAJ should be set up now."
msgstr ""
"Укажите, нужно ли сейчас выполнить настройку аутентификации через LDAP для "
"BioMAJ."

#. Type: string
#. Description
#: ../templates:5001
msgid "LDAP server:"
msgstr "Сервер LDAP:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Please enter the host name or IP address of the LDAP server to use for "
"authentication."
msgstr ""
"Введите имя узла или адрес сервера LDAP, который нужно использовать для "
"аутентификации."

#. Type: string
#. Description
#: ../templates:6001
msgid "LDAP DN:"
msgstr "LDAP DN:"

#. Type: string
#. Description
#: ../templates:6001
msgid "Please enter the Distinguished Name to use for LDAP authentication."
msgstr ""
"Укажите уникальное имя (Distinguished Name), которое нужно использовать при "
"аутентификации LDAP."

#. Type: string
#. Description
#: ../templates:7001
msgid "LDAP search filter:"
msgstr "Фильтр поиска в LDAP:"

#. Type: string
#. Description
#: ../templates:7001
msgid ""
"Please specify the LDAP search filter for biomaj-watcher. It can be left "
"empty if no filter is required."
msgstr ""
"Введите фильтр поиска в LDAP для biomaj-watcher. Если фильтр не "
"используется, то можно ничего не указывать."

#~ msgid "Want to configure LDAP now ?"
#~ msgstr "Настроить LDAP прямо сейчас?"

#~ msgid "Enter LDAP filter (empty if not needed):"
#~ msgstr "Введите фильтр LDAP (или оставьте пустым):"
