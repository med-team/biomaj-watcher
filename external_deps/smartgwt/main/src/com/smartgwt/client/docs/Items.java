
package com.smartgwt.client.docs;

/**
 * <h3>Form Items</h3>
 * Manipulating the items that belong to a form. <BR><br> An item manages an atomic value (eg a String, Number, Date, etc)
 * that appears as one of the properties in the overall form's values.  Some items exist purely for layout or appearance
 * purposes (eg SpacerItem) and do not manage a value.
 * @see com.smartgwt.client.widgets.form.DynamicForm#getItem
 * @see com.smartgwt.client.widgets.form.DynamicForm#getField
 * @see com.smartgwt.client.widgets.form.DynamicForm#getItems
 * @see com.smartgwt.client.widgets.form.DynamicForm#getFields
 * @see com.smartgwt.client.widgets.form.fields.ToolbarItem#getButtons
 */
public interface Items {
}
