
package com.smartgwt.client.docs;

/**
 * Rendering an object on the page
 * @see com.smartgwt.client.widgets.Canvas#isDrawn
 * @see com.smartgwt.client.widgets.Canvas#getInnerHTML
 * @see com.smartgwt.client.widgets.Canvas#draw
 * @see com.smartgwt.client.widgets.Canvas#isDirty
 * @see com.smartgwt.client.widgets.Canvas#markForRedraw
 * @see com.smartgwt.client.widgets.Canvas#redraw
 * @see com.smartgwt.client.widgets.form.fields.FormItem#getFieldName
 * @see com.smartgwt.client.widgets.form.fields.FormItem#getTitle
 * @see com.smartgwt.client.widgets.form.fields.FormItem#isDrawn
 * @see com.smartgwt.client.widgets.grid.ListGrid#markForRedraw
 * @see com.smartgwt.client.widgets.Canvas#getAutoDraw
 * @see com.smartgwt.client.widgets.Canvas#getRedrawOnResize
 */
public interface Drawing {
}
