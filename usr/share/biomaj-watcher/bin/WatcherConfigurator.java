import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


/**
 * Class that initializes the required properties for
 * BioMAJWatcher to work.
 * 
 * @author osallou
 *
 */
public class WatcherConfigurator {
	
	
	private static String biomajConf = "";
	
	private static String biomajRoot = "";
	
	private static final String MESSAGE_MAX ="30";
	
	
	public static void main(String[] args) {

		
		Map<String, String> params = new HashMap<String, String>();
		
		for (String arg : args) {
			String[] pair = arg.split(":=");
			if (pair.length > 1)
				params.put(pair[0], pair[1]);
			else
				params.put(pair[0], "");
		}

		biomajConf = params.get("bmajconf");
		biomajRoot = params.get("bmajroot");
		
		/*
		 * Context.xml
		 */
		System.out.println("Updating Context.xml...");
		try {
			PrintWriter pw = new PrintWriter(biomajConf + "/BmajWatcher.xml");
			
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			pw.println("<Context path=\"/BmajWatcher\" docBase=\"/usr/share/java/webapps/biomaj-watcher\"  reloadable=\"false\" allowLinking=\"true\">");

			pw.println("<Parameter name=\"ADMIN_LOGIN\" value=\"" + params.get("app_login") + "\" override=\"false\"/>");
			
			pw.println("<Parameter name=\"JOBS_LOCATION\" value=\"/etc/biomaj-watcher\" override=\"false\"/>");
			
			pw.println("<!-- ENV VARIABLES -->");
			pw.println("<Parameter name=\"BIOMAJ_ROOT\" value=\"" + biomajRoot + "\" override=\"false\"/>");
			
			pw.println("<!-- WEB INTERFACE CONF -->");
			pw.println("<Parameter name=\"MESSAGE_MAX\" value=\"" + MESSAGE_MAX + "\" override=\"false\"/>");

			pw.println("<Parameter name=\"USELOCAL\" value=\"1\" override=\"false\"/>");
			pw.println("<!-- LDAP CONF -->");
			if (params.get("use_ldap").equals("true"))
				pw.println("<Parameter name=\"USELDAP\" value=\"1\" override=\"false\"/>");
			else
				pw.println("<Parameter name=\"USELDAP\" value=\"0\" override=\"false\"/>");
			
			pw.println("<Parameter name=\"LDAPHOST\" value=\"" + params.get("ldap_host") + "\" override=\"false\"/>");
			pw.println("<Parameter name=\"LDAPDN\" value=\"" + params.get("ldap_dn") + "\" override=\"false\"/>");
			pw.println("<Parameter name=\"OPTFILTER\" value=\"" + params.get("opt_filter") + "\" override=\"false\"/>");
			
			pw.println("<!-- LOCAL/REMOTE EXECUTION -->");
			pw.println("<Parameter name=\"EXECUTION\" value=\"local\" override=\"false\"/>");

			pw.println("<!-- SSH CONFIG -->");
			pw.println("<Parameter name=\"SSH_AUTH_TYPE\" value=\"\" override=\"false\"/>");
			pw.println("<Parameter name=\"SSH_LOGIN\" value=\"\" override=\"false\"/>");
			pw.println("<Parameter name=\"SSH_PASSWD\" value=\"\" override=\"false\"/>");
			pw.println("<Parameter name=\"SSH_KEY_PATH\" value=\"\" override=\"false\"/>");
			pw.println("<Parameter name=\"SSH_PASSPHRASE\" value=\"\" override=\"false\"/>");
			pw.println("<Parameter name=\"SSH_HOSTS\" value=\"\" override=\"false\"/>");
			
			
			pw.println("</Context>");
			
			pw.close();
			
			String logBase = "/var/log/biomaj";

			pw = new PrintWriter(biomajConf + "/BmajWatcher#logs.xml");
			
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			pw.println("<Context docBase=\"" + logBase + "\" debug=\"0\" privileged=\"true\" allowLinking=\"true\">");
			pw.println("</Context>");
			
			pw.close();
		} catch (FileNotFoundException e) {
			System.err.println(e.toString());
		}

		System.out.println("Configuration complete");
	}
}

