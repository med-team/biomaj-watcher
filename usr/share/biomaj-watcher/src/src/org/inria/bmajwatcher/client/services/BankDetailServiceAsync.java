package org.inria.bmajwatcher.client.services;

import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BankDetailServiceAsync {

	public static final BankDetailServiceAsync INSTANCE = GWT.create(BankDetailService.class);
	
	void getDetailedBankInfo(String bankName, AsyncCallback<Map<String, String>> callback);

	void getFields(boolean restricted,
			AsyncCallback<Map<String, List<String>>> callback);

	void getTypes(String login, AsyncCallback<List<String>> callback);

	void getDependencies(String bankName,
			AsyncCallback<Map<String, String>> callback);

}
