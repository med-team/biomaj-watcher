package org.inria.bmajwatcher.client.ui.bank_editor;

import java.util.HashMap;
import java.util.Map;

import org.inria.bmajwatcher.client.ui.styles.MainCss;
import org.inria.bmajwatcher.client.ui.styles.Resources;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class DirectHttpParameterBuilder {

	private Map<String, String> parametersValue;
	private MainCss css = Resources.INSTANCE.mainCss();
	private Map<TextBox, TextBox> newValues;
	
	public DirectHttpParameterBuilder() {
		parametersValue = new HashMap<String, String>();
		newValues = new HashMap<TextBox, TextBox>();
	}
	
	public void addParameterValue(String param, String value) {
		parametersValue.put(param, value);
	}
	
	public Panel getComponent() {
		VerticalPanel res = new VerticalPanel();
		final VerticalPanel paramsPanel = new VerticalPanel();
		HorizontalPanel btnPanel = new HorizontalPanel();
		res.add(paramsPanel);
		res.add(btnPanel);
		
		for (String name : parametersValue.keySet()) {
			String[] split = name.split("__"); // type__paramName.value
			String paramName = split[1].substring(0, split[1].lastIndexOf('.'));
			String paramValue = parametersValue.get(name);
			if (paramValue.contains("_:_")) {
				String[] values = paramValue.split("_:_");
				for (String val : values) {
					paramsPanel.add(getParameterPanel(paramName, val));
				}
			} else {
				paramsPanel.add(getParameterPanel(paramName, paramValue));
			}
		}
		
		
		btnPanel.add(new Button("Add parameter", new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				paramsPanel.add(getParameterPanel("", ""));
			}
		}));
		
		return res;
	}
	
	private HorizontalPanel getParameterPanel(String name, String value) {
		final TextBox tbName = new TextBox();
		tbName.setWidth("300px");
		tbName.setText(name);
		TextBox tbValue = new TextBox();
		
		tbValue.setText(value);
		tbValue.setWidth("300px");
		
		VerticalPanel vp = new VerticalPanel();
		vp.addStyleName(css.directHttpParameterPanel());
		
		HorizontalPanel hpName = new HorizontalPanel();
		Label lblName = new Label("Name");
		lblName.setWidth("100px");
		hpName.add(lblName);
		hpName.add(tbName);
		hpName.setCellVerticalAlignment(lblName, HasVerticalAlignment.ALIGN_MIDDLE);
		
		HorizontalPanel hpValue = new HorizontalPanel();
		Label lblValue = new Label("Value");
		lblValue.setWidth("100px");
		hpValue.add(lblValue);
		hpValue.add(tbValue);
		hpValue.setCellVerticalAlignment(lblValue, HasVerticalAlignment.ALIGN_MIDDLE);
		
		vp.add(hpName);
		vp.add(hpValue);
		
		newValues.put(tbName, tbValue);
		
		final HorizontalPanel hp = new HorizontalPanel();
		PushButton btnRemove = new PushButton(new Image("images/edit-delete.png"));
		btnRemove.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				newValues.remove(tbName);
				hp.removeFromParent();
			}
		});
		btnRemove.addStyleName(css.removePropertyButton());
		
		
		hp.add(btnRemove);
		hp.add(vp);
		
		return hp;
	}
	
	public Map<String, String> getParameters() {
		String urlParams = "";
		Map<String, String> res = new HashMap<String, String>();
		for (TextBox tb : newValues.keySet()) {
			String val = tb.getText().trim();
			if (!val.isEmpty()) {
				String key = "misc__" + val + ".value";
				if (res.containsKey(key)) {
					res.put(key, res.get(key) + "_:_" + newValues.get(tb).getText());
				} else {
					urlParams += val + ",";
					res.put(key, newValues.get(tb).getText());
				}
			}
		}
		if (urlParams.length() > 0)
			res.put("misc__url.params", urlParams.substring(0, urlParams.lastIndexOf(',')));
		return res;
	}
	
}
