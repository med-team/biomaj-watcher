package org.inria.bmajwatcher.client.ui.confirmation;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Panel;

/**
 * Animation that fades the given component out.
 * 
 * @author rsabas
 *
 */
public class FadeOut extends Animation {
	
	private final Element toFade;
	
	
	public FadeOut(Panel p) {
		toFade = p.getElement();
	}
	
	public void fade(int duration) {
		run(duration);
	}

	@Override
	protected void onUpdate(double progress) {
		toFade.getStyle().setOpacity(1 - progress);
	}
	
	@Override
	protected void onComplete() {
		super.onComplete();
		toFade.getStyle().setDisplay(Display.NONE);
	}

}
