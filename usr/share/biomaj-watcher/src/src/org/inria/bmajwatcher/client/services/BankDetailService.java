package org.inria.bmajwatcher.client.services;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * 
 * Gets bank detailed information. 
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("bankDetailService")
public interface BankDetailService extends RemoteService {
	
	/**
	 * Returns a map that contains information on a given bank.
	 * 
	 * @param bankName bank name whose information is retrieved 
	 * @return
	 */
	Map<String, String> getDetailedBankInfo(String bankName);
	
	/**
	 * Return dependencies from given bank.
	 * 
	 * @param bankName
	 * @return
	 */
	Map<String, String> getDependencies(String bankName);
	
	/**
	 * Returns the field list for a bank organized by sections.
	 * 
	 * @param restricted whether to show restricted fields
	 * @return
	 */
	Map<String, List<String>> getFields(boolean restricted);
	
	/**
	 * Returns the types list for the banks belonging to given user group.
	 * 
	 * @return
	 */
	List<String> getTypes(String login);

}
