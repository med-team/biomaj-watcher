package org.inria.bmajwatcher.client.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Service that handles scheduling stuff.
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("schedulingService")
public interface SchedulingService extends RemoteService {

	/**
	 * Creates a trigger with a given name and a schedule represented
	 * by a cron expression.
	 * 
	 * @param label
	 * @param cron 
	 * @param user user the job belongs to
	 * 
	 * @return 0 if label already exists, -1 if syntax error, 1 if OK.
	 */
	public int createTrigger(String user, String label, String cron);
	
	/**
	 * Deletes the trigger with the corresponding label and all
	 * the corresponding jobs.
	 * 
	 * @param label
	 */
	public void deleteTrigger(String label);
	
	/**
	 * Renames the <code>oldName</code> trigger with the <code>newName</code>.
	 * 
	 * @param oldName
	 * @param newName
	 * 
	 * @return false is name already exists, true if OK
	 */
	public boolean renameTrigger(String oldName, String newName);
	
	/**
	 * Sets the cron expression of the trigger represented by <code>name</code>
	 * to <code>cron</code>.
	 * 
	 * @param name
	 * @param cron
	 * 
	 * @return true if successful
	 */
	public boolean setCron(String name, String cron);
	
	/**
	 * Adds a bank to the given trigger.
	 * 
	 * @param bankName
	 * @param triggerName
	 */
	public void addBanksToTrigger(String[] bankNames, String triggerName);
	
	/**
	 * Removes a bank list from the given trigger.
	 * 
	 * @param bankNames
	 * @param triggerName
	 */
	public void removeBanksFromTrigger(String[] bankNames, String triggerName);
	
	/**
	 * Returns the job list.
	 * 
	 * @param user user jobs belong to
	 * @return
	 */
	public Map<String, List<String>> getJobs(String user);
	
	/**
	 * Returns a list of dates within the specified range together
	 * with the job it is related to.
	 * 
	 * @param user user jobs belong to
	 * @param current
	 * @param range
	 * @return
	 */
	public Map<Date, String> getJobsDates(String user, Date current, int range);
	
	/**
	 * Returns whethers scheduler is started.
	 * @return
	 */
	public boolean isStarted();
	
	/**
	 * Stop scheduler. Returns false if an error was encountered.
	 * @return
	 */
	public boolean stop();
	
	/**
	 * Starts scheduler. Returns false if an error was encountered.
	 * @return
	 */
	public boolean start();
	
}
