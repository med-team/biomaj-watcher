package org.inria.bmajwatcher.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.bmajwatcher.server.data.ErrorRetriever;


public class ErrorListingService extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		fetchData(resp.getWriter(), req.getParameter("auth_key"));
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		fetchData(resp.getWriter(), req.getParameter("auth_key"));
	}
	
	private void fetchData(PrintWriter out, String userKey) {
		if (userKey != null && !userKey.trim().isEmpty()) {
			String maxMessages = getServletContext().getInitParameter("MESSAGE_MAX");
			out.write(ErrorRetriever.getMessages(maxMessages, userKey));
			out.flush();
		}
	}
	
}
