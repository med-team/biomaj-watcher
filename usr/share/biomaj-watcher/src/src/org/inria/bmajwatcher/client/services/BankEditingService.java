package org.inria.bmajwatcher.client.services;

import java.util.Collection;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Service for bank properties edition.
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("bankEditingService")
public interface BankEditingService extends RemoteService {
	
	/**
	 * Returns the bank properties contained in the associated .properties file.
	 * 
	 * @param login user bank belongs to
	 * @param bankName
	 * @param type whether to load from local, from remote, or create an empty bank
	 * @param included list that contains the names of the included properties files
	 * @return
	 */
	public Map<String, String> getBankProperties(String login, String bankName, String type, Collection<String> included);
	
	/**
	 * Returns the bank list for the given user.
	 * 
	 * @return
	 */
	public Collection<String> getBankList(String login);
	
	/**
	 * Returns the list of files that can be included into a bank.
	 * These files are located in [process_directory]/[user]/include.
	 * 
	 * @param user user login include properties belong to
	 * @return
	 */
	public Collection<String> getIncludePropertiesList(String user);
	
	/**
	 * Returns a bank list from a distant repository via HTTP.
	 * 
	 * @param url
	 * @return
	 */
	public Collection<String> getRemoteBankList(String url);
	
	/**
	 * Returns for a bank, a map that contains for each included file (key) the
	 * associated properties.
	 * 
	 * @param login user login whose banks are retrieved
	 * @param bankName bank whose included properties have to be retrieved
	 * @param included additionnal included properties
	 * @return
	 */
	public Map<String, Map<String, String>> getIncludedProperties(String login, String bankName, Collection<String> included);
	
	/**
	 * Create the properties file for the given bank
	 * with the specified new properties.
	 * 
	 * @param login user bank belongs to<
	 * @param type local or remote bank to be saved
	 * @param bankName bank properties file to update
	 * @param newName output name
	 * @param newProps new properties to write
	 * @param runUpdate whether to run the update after saving
	 * 
	 * @return 0 if wrong file name, -1 if any other error, 1 if ok
	 */
	public int save(String login, String type, String bankName, String newName, Map<String, String> newProps, boolean runUpdate);
	
	/**
	 * Returns a list of empty or inherited property from the given pattern.
	 * 
	 * @param pattern
	 * @return
	 */
	public Map<String, Map<String, String>> getEmptyBank(String pattern);
	
	/**
	 * Returns for each property a description text.
	 * 
	 * @return
	 */
	public Map<String, String> getPropertyHelp();
	
	/**
	 * Returns the list of the mandatory bank properties.
	 * 
	 * @return
	 */
	public Collection<String> getMandatoryProperties();
	
	/**
	 * Returns the prefixed given key as defined on the server.
	 * 
	 * @param key
	 * @return
	 */
	public String getPrefixedKey(String key);
	
	/**
	 * Returns a list of the process executables located in the biomaj process directory.
	 * 
	 * @return
	 */
	public Collection<String> getProcessList();
	
	/**
	 * Returns the description corresponding to the given process.
	 * 
	 * @param process
	 * @return
	 */
	public String getProcessDescription(String process);
	
	/**
	 * Tests whether the given file exists for the given user group.
	 * 
	 * @param login user login to retrieve group from
	 * @param fileName file name relative to the workflow directoy
	 * @return
	 */
	public boolean propertiesFileExists(String login, String fileName);
	
}
