package org.inria.bmajwatcher.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Authentication service.
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("authenticationService")
public interface AuthenticationService extends RemoteService {
	
	/**
	 * Authentication method that takes user login and password and
	 * returns wether authentication failed or succeed.
	 *  
	 * @param login
	 * @param passwd
	 * @return
	 */
	public boolean authenticate(String login, String passwd);
	
	/**
	 * Returns logged user login and type in the form <login>:<type>.
	 * Type can be user or admin.
	 * 
	 * @return
	 */
	public String getUserAccess();
	
	/**
	 * Removes user from session.
	 */
	public void logout();
}
