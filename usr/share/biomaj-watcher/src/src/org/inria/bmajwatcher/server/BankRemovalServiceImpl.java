package org.inria.bmajwatcher.server;

import java.util.ArrayList;
import java.util.List;

import org.inria.biomaj.session.bank.BiomajSQLQuerier;
import org.inria.biomaj.session.bank.ProductionDirectory;
import org.inria.bmajwatcher.client.services.BankRemovalService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class BankRemovalServiceImpl extends RemoteServiceServlet implements BankRemovalService {

	private static final long serialVersionUID = 1L;
	
	private List<ProductionDirectory> prods;

	@Override
	public void deleteDirectories(List<String> dirs, String bankName, boolean keepProd) {
		new BiomajLauncherServiceImpl().removeDirs(dirs, bankName, keepProd);
	}
	

	@Override
	public List<String> getDirectories(String bankName) {
		prods = BiomajSQLQuerier.getAvailableProductionDirectories(bankName);
		
		List<String> res = new ArrayList<String>();
		for (ProductionDirectory dir : prods)
			res.add(dir.getPath());
		
		return res;
	}


	@Override
	public void deleteBank(String bankName, boolean noHistory, boolean keepProd) {
		new BiomajLauncherServiceImpl().removeAll(bankName, noHistory, keepProd);
		
	}

}
