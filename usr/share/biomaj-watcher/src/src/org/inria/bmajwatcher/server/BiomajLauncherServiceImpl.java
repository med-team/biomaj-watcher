package org.inria.bmajwatcher.server;

import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.inria.bmajwatcher.client.services.BiomajLauncherService;
import org.inria.bmajwatcher.server.launcher.LauncherFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class BiomajLauncherServiceImpl extends RemoteServiceServlet implements BiomajLauncherService {

	private static final long serialVersionUID = 1L;
	private static ServletContext context = null;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		context = getServletContext();
	}
	
	@Override
	public boolean startUpdate(String bankName) {
		return LauncherFactory.getLauncher(context).startUpdate(bankName);
	}

	@Override
	public boolean startUpdateFromScratch(String bankName) {
		return LauncherFactory.getLauncher(context).startUpdateFromScratch(bankName);
	}

	@Override
	public boolean stopUpdate(String bankName) {
		return false;
	}
	
	@Override
	public boolean rebuild(String bankName) {
		return LauncherFactory.getLauncher(context).rebuild(bankName);
	}

	@Override
	public boolean startUpdateNew(String bankName) {
		return LauncherFactory.getLauncher(context).startUpdateNew(bankName);
	}
	
	public boolean removeAll(String bankName, boolean noHistory, boolean keepProd) {
		return LauncherFactory.getLauncher(context).removeAll(bankName, noHistory, keepProd);
	}
	
	public boolean removeDirs(List<String> paths, String bankName, boolean keepProd) {
		return LauncherFactory.getLauncher(context).removeDirs(paths, bankName, keepProd);
	}
	
}
