package org.inria.bmajwatcher.client.services;

import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


/**
 * Service for managing (CRUD operations) saved remote repositories. 
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("siteManagerService")
public interface SiteManagerService extends RemoteService {

	/**
	 * Returns sites list as a map (site url => site description)
	 * 
	 * @return
	 */
	public Map<String, String> getSites();
	
	/**
	 * Delete site referenced by given url.
	 * @param url
	 * 
	 * @return false if operation failed
	 */
	public boolean deleteSite(String url);
	
	/**
	 * Updates site referenced by refUrl with new values.
	 * 
	 * @param refUrl
	 * @param newUrl
	 * @param newDescription
	 * 
	 * @return false if operation failed
	 */
	public boolean updateSite(String refUrl, String newUrl, String newDescription);
	
	/**
	 * Add new site with given url and description
	 * @param url
	 * @param description
	 * 
	 * @return false if operation failed (IO error or site already exists)
	 */
	public boolean addSite(String url, String description);
}
