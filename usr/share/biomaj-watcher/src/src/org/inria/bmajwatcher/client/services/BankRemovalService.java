package org.inria.bmajwatcher.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Provides methods for bank deletion.
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("bankRemovalService")
public interface BankRemovalService extends RemoteService {
	
	/**
	 * Returns list of versions for a given bank.
	 * 
	 * @param bankName
	 * @return
	 */
	public List<String> getDirectories(String bankName);
	
	/**
	 * Remove the given production directories from the file system and db.
	 * 
	 * @param versions
	 * @param bankName
	 * @param keepProd whether to delete the production directories
	 */
	public void deleteDirectories(List<String> versions, String bankName, boolean keepProd);
	
	/**
	 * Remove the all the production directories and stored sessions from db.
	 * 
	 * @param bankName
	 * @param noHistory whether to delete all bank history from db
	 * @param keepProd whether to delete the production directories
	 */
	public void deleteBank(String bankName, boolean noHistory, boolean keepProd);
}
