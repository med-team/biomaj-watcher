package org.inria.bmajwatcher.client.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SchedulingServiceAsync {

	public static final SchedulingServiceAsync INSTANCE = GWT.create(SchedulingService.class);

	void getJobs(String user, AsyncCallback<Map<String, List<String>>> callback);

	void getJobsDates(String user, Date current, int range,
			AsyncCallback<Map<Date, String>> callback);

	void createTrigger(String user, String label, String cron,
			AsyncCallback<Integer> callback);

	void deleteTrigger(String label, AsyncCallback<Void> callback);

	void renameTrigger(String oldName, String newName,
			AsyncCallback<Boolean> callback);

	void setCron(String name, String cron,
			AsyncCallback<Boolean> callback);

	void removeBanksFromTrigger(String[] bankNames, 
			String triggerName, AsyncCallback<Void> callback);

	void addBanksToTrigger(String[] bankNames, String triggerName,
			AsyncCallback<Void> callback);

	void isStarted(AsyncCallback<Boolean> callback);

	void stop(AsyncCallback<Boolean> callback);

	void start(AsyncCallback<Boolean> callback);

}
