package org.inria.bmajwatcher.client;

import org.inria.bmajwatcher.client.ui.HomePage;
import org.inria.bmajwatcher.client.ui.confirmation.MessagePanel;
import org.inria.bmajwatcher.client.ui.styles.Resources;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.LineChart;
import com.google.gwt.visualization.client.visualizations.OrgChart;
import com.google.gwt.visualization.client.visualizations.PieChart;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class BmajWatcher implements EntryPoint {
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		Resources.INSTANCE.mainCss().ensureInjected();
		
		Runnable onLoadCallback = new Runnable() {
			
			@Override
			public void run() {
				RootPanel.get().add(MessagePanel.getInstance());
				RootPanel.get().add(new HomePage());
			}
		};
		
		VisualizationUtils.loadVisualizationApi(onLoadCallback, LineChart.PACKAGE, PieChart.PACKAGE, OrgChart.PACKAGE);
		
	}
}
