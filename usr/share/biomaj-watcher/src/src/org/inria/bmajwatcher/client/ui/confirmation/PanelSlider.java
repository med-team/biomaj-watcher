package org.inria.bmajwatcher.client.ui.confirmation;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Panel;

/**
 * Animation that slides the given panel from to right to the
 * middle of the screen.
 * 
 * @author rsabas
 *
 */
public class PanelSlider extends Animation {
	
	private final Element toSlide;
	private int distanceToSlide = 0;
	private int initialPosition = 0;
	private int initialWidth = 0;
	
	private Notifiable toNotify = null;
	
	
	public PanelSlider(Panel p) {
		toSlide = p.getElement();
		
	}
	
	public void slideTo(int x, int duration) {
		initialPosition = toSlide.getOffsetLeft();
		initialWidth = toSlide.getOffsetWidth();
		distanceToSlide = x;
		run(duration);
	}

	@Override
	protected void onUpdate(double progress) {
		double newPos = initialPosition - (progress * distanceToSlide);
		toSlide.getStyle().setLeft(newPos, Unit.PX);
		toSlide.getStyle().setWidth(initialWidth + (progress * distanceToSlide), Unit.PX);
	}
	
	@Override
	protected void onComplete() {
		super.onComplete();
		if (toNotify != null) {
			toNotify.fire();
		}
	}
	
	public void setNotifiable(Notifiable r) {
		toNotify = r;
	}
}
