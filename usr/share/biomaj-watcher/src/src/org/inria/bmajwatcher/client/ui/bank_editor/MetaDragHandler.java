package org.inria.bmajwatcher.client.ui.bank_editor;

import com.allen_sauer.gwt.dnd.client.DragEndEvent;
import com.allen_sauer.gwt.dnd.client.DragHandler;
import com.allen_sauer.gwt.dnd.client.DragStartEvent;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.google.gwt.user.client.ui.ComplexPanel;

/**
 * Updates processEditor after each drag operation on a meta element to
 * delete orphan panels.
 * 
 * @author rsabas
 */
public class MetaDragHandler implements DragHandler {

	private ComplexPanel parent; // content of META
	private ProcessEditor editor;
	
	public MetaDragHandler(ProcessEditor pe) {
		editor = pe;
	}
	
	@Override
	public void onDragEnd(DragEndEvent event) {
		editor.cleanMetaPanel(parent);
	}

	@Override
	public void onDragStart(DragStartEvent event) {
		if (event.getSource() instanceof ComplexPanel) {
			parent = (ComplexPanel) ((ComplexPanel) event.getSource()).getParent();
		}
	}

	@Override
	public void onPreviewDragEnd(DragEndEvent event) throws VetoDragException {
	}

	@Override
	public void onPreviewDragStart(DragStartEvent event) throws VetoDragException {
		
	}

}
