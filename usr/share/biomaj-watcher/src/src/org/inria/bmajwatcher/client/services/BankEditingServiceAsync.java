package org.inria.bmajwatcher.client.services;

import java.util.Collection;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BankEditingServiceAsync {
	
	public static final BankEditingServiceAsync INSTANCE = GWT.create(BankEditingService.class);

	void getBankProperties(String login, String bankName, String type, Collection<String> included, AsyncCallback<Map<String, String>> callback);

	void getBankList(String login, AsyncCallback<Collection<String>> callback);

	void getIncludedProperties(String login, String bankName, Collection<String> included,
			AsyncCallback<Map<String, Map<String, String>>> callback);

	void getRemoteBankList(String url, AsyncCallback<Collection<String>> callback);

	void save(String login, String type, String bankName, String newName, Map<String, String> newProps, boolean runUpdate,
			AsyncCallback<Integer> callback);

	void getEmptyBank(String pattern,
			AsyncCallback<Map<String,  Map<String, String>>> callback);

	void getPropertyHelp(AsyncCallback<Map<String, String>> callback);

	void getMandatoryProperties(AsyncCallback<Collection<String>> callback);

	void getPrefixedKey(String key, AsyncCallback<String> callback);

	void getIncludePropertiesList(String user, AsyncCallback<Collection<String>> callback);

	void getProcessList(AsyncCallback<Collection<String>> callback);

	void getProcessDescription(String process, AsyncCallback<String> callback);

	void propertiesFileExists(String login, String fileName, AsyncCallback<Boolean> callback);
}
