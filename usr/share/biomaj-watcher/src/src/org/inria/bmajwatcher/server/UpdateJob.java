package org.inria.bmajwatcher.server;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateJob implements Job {
	
	private static Logger log = Logger.getLogger(UpdateJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		String banksToUpdate = context.getJobDetail().getDescription();
		if (!banksToUpdate.isEmpty()) {
			if(MaintenanceServiceImpl.getMode()) {
				log.info("Maintenance mode, skipping " + banksToUpdate);
				return;
			}
			log.info("Updating " + banksToUpdate);
			new BiomajLauncherServiceImpl().startUpdate(banksToUpdate);
		}
	}

}
