package org.inria.bmajwatcher.client.services;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BankRemovalServiceAsync {

	public static final BankRemovalServiceAsync INSTANCE = GWT.create(BankRemovalService.class);
	
	void getDirectories(String bankName, AsyncCallback<List<String>> callback);

	void deleteBank(String bankName, boolean noHistory, boolean keepProd, AsyncCallback<Void> callback);

	void deleteDirectories(List<String> versions, String bankName,
			boolean keepProd, AsyncCallback<Void> callback);

}
