package org.inria.bmajwatcher.client.ui.bank_editor;

import com.allen_sauer.gwt.dnd.client.DragEndEvent;
import com.allen_sauer.gwt.dnd.client.DragHandler;
import com.allen_sauer.gwt.dnd.client.DragStartEvent;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.google.gwt.user.client.ui.ComplexPanel;

/**
 * Updates processEditor after each drag operation on a process to
 * delete orphan panels.
 * 
 * @author rsabas
 */
public class ProcessDragHandler implements DragHandler {

	/*
	 * Each panel is composed of two panel, the content panel that contains
	 * the child panels, the buttons panel that contains the buttons.
	 * The direct parent of a panel is the content panel. What we want to
	 * remove is the parent panel of this panel.
	 * 
	 * An xml representation would be :
	 * 
	 *	<BLOCK>
	 *  	<Content>
	 *   		<META>
	 *   			<Content>
	 *   				<PROCESS>
	 *   					<Content>
	 *   					</Content>
	 *   					<Buttons>
	 *   					</Buttons>
	 *   				</PROCESS>
	 *   			</Content>
	 *   			<Buttons>
	 *   			</Buttons>
	 *   		</META>
	 *   	</Content>
	 *   	<Buttons>
	 *   	</Buttons>
	 * 	</BLOCK>
	 * 
	 * 
	 */
	private ComplexPanel parent; // content of META
	private ProcessEditor editor;
	
	public ProcessDragHandler(ProcessEditor pe) {
		editor = pe;
	}
	
	@Override
	public void onDragEnd(DragEndEvent event) {
		editor.cleanProcessPanel(parent);
	}

	@Override
	public void onDragStart(DragStartEvent event) {
		if (event.getSource() instanceof ComplexPanel) {
			parent = (ComplexPanel) ((ComplexPanel) event.getSource()).getParent();
		}
	}

	@Override
	public void onPreviewDragEnd(DragEndEvent event) throws VetoDragException {
	}

	@Override
	public void onPreviewDragStart(DragStartEvent event) throws VetoDragException {
		
	}

}
