package org.inria.bmajwatcher.client.ui.styles;

import com.google.gwt.resources.client.CssResource;

/**
 * main.css styles definition binding.
 * 
 * @author rsabas
 *
 */
public interface MainCss extends CssResource {
	
	String footer();
	String scheduler();
	String loginPane();
	String tabPane();
	String bankDetail();
	String toolStrip();
	String taskEditor();
	String test();
	String cellLabel();
	String cellValue();
	String detailGrid();
	String evenCell();
	String oddCell();
	String graphMenu();
	String formLabel();
	String formText();
	String formBtn();
	String main();
	String center();
	String right();
	String title();
	String checkBox();
	String deletePanel();
	String calendar();
	String datePicker();
	String apptDetail();
	String imgButton();
	String bankEditor();
	String bankEditorPanel();
	String bankEditorSubPanel();
	String blockPanel();
	String metaPanel();
	String processPanel();
	String processEditorButton();
	String processDetail();
	String editorPanelTitle();
	String helpIcon();
	String processEditorPanel();
	String processEditorTitleTxt();
	String processEditorContainer();
	String processEditorLabel();
	String bankEditorTitle();
	String bankEditorTextBox();
	String bankEditorErrorWidget();
	String removePropertyButton();
	String propsParameter();
	String buttonMenu();
	String warningPanel();
	String imgPointer();
	String processParameter();
	String includeFilePanel();
	String suggestBox();
	String copyMenuStyle();
	String subCbPanel();
	String bankEditorTopPanel();
	String bankEditorMainPanel();
	String bankPanel();
	String bankEditorSmallTitle();
	String selectBankLabel();
	String bankEditorTitleValue();
	String cronCreationPanel();
	String typeHierarchyDialog();
	String typePanel();
	String toolBarMenuSmall();
	String siteManagerDetail();
	String siteManagerButtons();
	String siteManagerList();
	String userInfoMainPanel();
	String userInfoSubPanel();
	String userInfoTitle();
	String userTreeTitle();
	String userInfoSection();
	String userInfoTextBox();
	String userInfoGroupButton();
	String userTreeRefresh();
	String userInfoTreePanel();
	String searchBox();
	String siteManagerTitle();
	String directHttpParameterPanel();
	String dependencyTopPanel();
	String dependencyPanel();
	String dependency100Panel();
	String dependencyMainPanel();
	String dependencyTitle();
	String singleCellValue();
	String propertyDescriptionPanel();
	String propertyListPopup();
	String propertyHelpOptionPanel();
	String propertyHelpDescPanel();
	String propertyHelpSubTitle();
	String propertyHelpSearchTitle();
	String propertyHelpCategoryPanel();
	String propertyHelpSortPanel();
	String propertyHelpSearchPanel();
	String propertyHelpInheritancePanel();
	String messagePanel();
	String messageSubPanel();
	String schedulerButtons();
}

