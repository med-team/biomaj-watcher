package org.inria.bmajwatcher.client.ui.confirmation;


/**
 * Interface used by animation classes to notify
 * animation end.
 *  
 * @author rsabas
 *
 */
public interface Notifiable {
	
	public void fire();
}
