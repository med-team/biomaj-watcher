package org.inria.bmajwatcher.client.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UserManagerServiceAsync {
	
	public static final UserManagerServiceAsync INSTANCE = GWT.create(UserManagerService.class);

	void deleteUser(String login, AsyncCallback<Boolean> callback);

	void addGroup(String groupName, AsyncCallback<Boolean> callback);

	void updateGroup(String oldName, String newName,
			AsyncCallback<Boolean> callback);

	void deleteGroup(String groupName, AsyncCallback<Boolean> callback);

	void setUserPassword(String login, String password,
			AsyncCallback<Boolean> callback);

	void addUserToGroup(String login, String group,
			AsyncCallback<Boolean> callback);

	void removeUserFromGroup(String login, String group,
			AsyncCallback<Boolean> callback);

	void addUser(String login, Set<String> groups,
			String authType, String mail, AsyncCallback<Boolean> callback);

	void getUsers(AsyncCallback<Map<String, Map<String, String>>> callback);

	void getGroups(AsyncCallback<List<String>> callback);

	void getAdminLogin(AsyncCallback<String> callback);

	void resetUID(String login, AsyncCallback<Boolean> callback);

	void getUserKey(String login, AsyncCallback<String> callback);

	void changeBankVisibility(String bank, boolean visibility, AsyncCallback<Boolean> callback);

	void getUserLoginFromKey(String key, AsyncCallback<String> callback);

	void setUserMail(String login, String newMail,
			AsyncCallback<Boolean> callback);

	void getLogins(AsyncCallback<List<String>> callback);

	void changeBankOwner(String bankName, String newOwnerLogin,
			AsyncCallback<Boolean> callback);

}
