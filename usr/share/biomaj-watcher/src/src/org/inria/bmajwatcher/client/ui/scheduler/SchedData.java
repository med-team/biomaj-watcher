package org.inria.bmajwatcher.client.ui.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.Random;

public class SchedData {

	private Map<String, List<String>> data = new HashMap<String, List<String>>();
	
	private static SchedData instance = new SchedData();
	
	/**
	 * Creates a task with given name and cron expression and
	 * returns the related id in the map.
	 * 
	 * @param name
	 * @param cron
	 * @return
	 */
	public String addTask(String user, String name, String cron) {
		String id = "id" + Random.nextInt();
		List<String> list = new ArrayList<String>(2);
		list.add(name);
		list.add(cron);
		list.add(user);
		data.put(id, list);
		
		return id;
	}
	
	public void changeName(String id, String name) {
		List<String> list = new ArrayList<String>(2);
		list.add(name);
		list.add(data.get(id).get(1));
		data.put(id, list);
	}
	
	public void changeCron(String id, String cron) {
		List<String> list = new ArrayList<String>(2);
		list.add(data.get(id).get(0));
		list.add(cron);
		data.put(id, list);
	}
	
	public void deleteTask(String id) {
		data.remove(id);
	}
	
	public String getName(String id) {
		return data.get(id).get(0);
	}
	
	public String getCron(String id) {
		return data.get(id).get(1);
	}

	public boolean jobExists(String user, String name) {
		for (List<String> list : data.values())
			if (list.get(0).equals(name) && list.get(2).equals(user))
				return true;
		return false;
	}
	
	public static SchedData getInstance() {
		return instance;
	}
	
	public void clear() {
		data.clear();
	}
	
}
