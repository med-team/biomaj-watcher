package org.inria.bmajwatcher.server;


import org.inria.bmajwatcher.client.services.MaintenanceService;
import org.apache.log4j.Logger;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * Service that handles authentication requests.
 * 
 * @author rsabas
 * 
 */
public class MaintenanceServiceImpl extends RemoteServiceServlet implements
		MaintenanceService {

	private static final long serialVersionUID = 1L;
	private static final String LOGGED = "logged";
	private static Logger log = Logger.getLogger(MaintenanceServiceImpl.class);
	
	private static boolean maintenance = false;
	
	public static boolean getMode() {
		return maintenance;
	}
	
	public boolean getMaintenanceMode() {
		return maintenance;
	}
	public void setMaintenanceMode(boolean mode) {
		maintenance = mode;
		
	}

	

}
