package org.inria.bmajwatcher.client.ui.confirmation;

import org.inria.bmajwatcher.client.ui.styles.Resources;

import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

/**
 * Panel that slides from the sides and displays messages such as confirmation
 * message.
 * 
 * @author rsabas
 * 
 */
public class MessagePanel extends HorizontalPanel implements Notifiable {

	private static MessagePanel instance = new MessagePanel();
	private FadeOut fade = new FadeOut(this);

	private MessagePanel() {
		setStylePrimaryName(Resources.INSTANCE.mainCss().messagePanel());
		setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
	}
	
	public static void showMessage(String message) {
		instance.clear();
		Label content = new Label(message);
		instance.getElement().getStyle().setDisplay(Display.INLINE);
		HorizontalPanel container = new HorizontalPanel();
		container.addStyleName(Resources.INSTANCE.mainCss().messageSubPanel());
		container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		container.add(content);
		instance.add(container);
		PanelSlider slider = new PanelSlider(container);
		slider.setNotifiable(instance);
		slider.slideTo(200, 1500);
		
	}
	
	public static MessagePanel getInstance() {
		return instance;
	}
	
	/*
	 * Fired by slider when slide ends.
	 * Starts fading out.
	 * 
	 * (non-Javadoc)
	 * @see org.inria.bmajwatcher.client.ui.confirmation.Notifiable#fire()
	 */
	@Override
	public void fire() {
		fade.fade(2000);
	}

}
