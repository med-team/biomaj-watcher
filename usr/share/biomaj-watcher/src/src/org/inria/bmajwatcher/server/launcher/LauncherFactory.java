package org.inria.bmajwatcher.server.launcher;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

/**
 * Provides a command launcher depending on the configuration.
 * 
 * @author rsabas
 *
 */
public class LauncherFactory {
	
	private static Logger log = Logger.getLogger(LauncherFactory.class);
	
	private static LocalLauncher local = null;
	private static RemoteLauncher remote = null;
	
	
	/**
	 * Returns a launcher depending on configuration in web.xml/context.xml.
	 * 
	 * @return
	 */
	public static Launcher getLauncher(ServletContext context) {
		
		String mode = context.getInitParameter("EXECUTION");
		if (mode != null) {
			if (mode.equals("local")) {
				if (local == null)
					local = new LocalLauncher(context);
				log.debug("Return local launcher");
				return local;
			} else if (mode.equals("remote")) {
				if (remote == null)
					remote = new RemoteLauncher(context);
				log.debug("Return remote launcher");
				return remote;
			} else {
				log.warn("Unknown execution mode : '" + mode + "'. Set to default : 'local'");
			}
		} else {
			log.warn("Could not find execution mode. Default is 'local'.");
		}
		
		// Default value
		if (local == null)
			local = new LocalLauncher(context);
		log.debug("Default mode. Return local launcher");
		return local;
	}

}
