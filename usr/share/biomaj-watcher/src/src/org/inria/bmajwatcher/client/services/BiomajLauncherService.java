package org.inria.bmajwatcher.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Service that executes or stops biomaj updates. 
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("biomajLauncherService")
public interface BiomajLauncherService extends RemoteService {
	
	/**
	 * Starts the update of the given bank.
	 * biomaj.sh --update bankName
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean startUpdate(String bankName);
	
	/**
	 * Starts the update of the given bank from scratch
	 * biomaj.sh --update --fromscratch bankName
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean startUpdateFromScratch(String bankName);
	
	/**
	 * Stops the update of the given bank.
	 * 
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean stopUpdate(String bankName);
	
	/**
	 * Starts an update with --new option.
	 * biomaj.sh --update bankName --new
	 * 
	 * @param banName
	 * @return
	 */
	public boolean startUpdateNew(String bankName);
	
	
	/**
	 * Rebuilds the given bank.
	 * biomaj.sh --rebuild bankName
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean rebuild(String bankName);
}
