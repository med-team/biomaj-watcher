package org.inria.bmajwatcher.client.services;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MaintenanceServiceAsync {
	
	public static final MaintenanceServiceAsync INSTANCE = GWT.create(MaintenanceService.class);

	void getMaintenanceMode(AsyncCallback<Boolean> callback);

	void setMaintenanceMode(boolean mode, AsyncCallback<Void> callback);

}
