package org.inria.bmajwatcher.client.ui.styles;

import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.CellList.Style;

public interface PropertyCellStyle extends CellList.Resources {
	
	/**
	 * Styles to define are : 
	 *  - cellListEvenItem
	 *  - cellListOddItem
	 *  - cellListSelectedItem
	 *  - cellListWidget
	 */
	@Source("propertyCell.css")
	@Override
	public Style cellListStyle();
	
}