package org.inria.bmajwatcher.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Maintenance service. If activated, it disables the scheduling tasks.
 * 
 * @author rsabas
 *
 */
@RemoteServiceRelativePath("maintenanceService")
public interface MaintenanceService extends RemoteService {
	
	/**
	 * Check current mode for maintenance
	 * @return Current maintenance mode
	 */
	public boolean getMaintenanceMode();
	
	/**
	 * Set maintenance mode on of off.
	 * @param mode mode to set
	 * @return
	 */
	public void setMaintenanceMode(boolean mode);
	
}
