package org.inria.bmajwatcher.server.launcher;


import java.util.List;

public interface Launcher {

	/**
	 * Start normal update.
	 * biomaj.sh --update
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean startUpdate(String bankName);

	/**
	 * Start update from scratch.
	 * biomaj.sh --update bankName --fromscratch
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean startUpdateFromScratch(String bankName);

	/**
	 * Not used.
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean stopUpdate(String bankName);
	
	/**
	 * Rebuid bank.
	 * biomaj.sh --rebuild bankName
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean rebuild(String bankName);

	/**
	 * Start new update.
	 * biomaj.sh --update bankName --new
	 * 
	 * @param bankName
	 * @return
	 */
	public boolean startUpdateNew(String bankName);
	
	/**
	 * Remove bank and all its production directories.
	 * biomaj.sh --remove bankName --all
	 * 
	 * @param bankName
	 * @param noHistory whether to keep bank history on db
	 * @param keepProd whether to delete or not the production directories
	 * @return
	 */
	public boolean removeAll(String bankName, boolean noHistory, boolean keepPord);
	
	/**
	 * Remove specified production directories from bank.
	 * biomaj.sh --remove bankName --paths path1 path2 ...
	 * 
	 * @param paths production directories paths to remove
	 * @param bankName
	 * @param keepProd whether to delete or not the production directories
	 * @return
	 */
	public boolean removeDirs(List<String> paths, String bankName, boolean keepProd);
}
