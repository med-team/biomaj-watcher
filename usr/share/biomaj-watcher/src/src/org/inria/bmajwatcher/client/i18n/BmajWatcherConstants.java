package org.inria.bmajwatcher.client.i18n;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Constants;

/**
 * Interface for internationalization constants handling.
 * Each methods returns the value of the corresponding attribute in
 * .properties files.
 * See http://code.google.com/p/google-web-toolkit-doc-1-5/wiki/GettingStartedI18n
 * @author rsabas
 *
 */
public interface BmajWatcherConstants extends Constants {
	
	public static final BmajWatcherConstants INSTANCE = GWT.create(BmajWatcherConstants.class);
	
	String type();
	String name();
	String release();
	String session();
	String status();
	String login();
	String submit();
	String userName();
	String passwd();
	String banksStatus();
	String statistics();
	String bankSessions();
	String bankOverview();
	String bankStats();
	String newTask();
	String emptyGridMessage();
	String remove();
	String editTask();
	String deleteTask();
	String saveTask();
	String rpcFailure();
	String logout();
	String updateStart();
	String updateStop();
	String updateFromScratch();
	String filterType();
	String search();
	String accessDenied();
	String preprocesses();
	String postprocesses();
	String prodDirs();
	String properties();
	String failure();
	String size();
	String bandwidth();
	String fileCount();
	String fileTurnover();
	String updateDuration();
	String formats();
	String errors();
	String bank();
	String message();
	String source();
	String date();
	String log();
	String mandatoryDescription();
	String commonDescription();
	String exhaustiveDescription();
	String globalWarning();
	String includeWarning();
	String globalForbidden();
}
