package org.inria.bmajwatcher.server.utils;

import java.util.UUID;


public class Utils {

	/**
	 * Returns hexadecimal string representation of a given byte array.
	 * 
	 * @param buf
	 * @return
	 */
	public static String getHexString(byte[] buf) {
		char[] TAB_BYTE_HEX = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
				'9', 'a', 'b', 'c', 'd', 'e', 'f' };

		StringBuilder sb = new StringBuilder(buf.length * 2);

		for (int i = 0; i < buf.length; i++) {
			sb.append(TAB_BYTE_HEX[(buf[i] >>> 4) & 0xf]);
			sb.append(TAB_BYTE_HEX[buf[i] & 0x0f]);
		}
		return sb.toString();
	}
	
	/**
	 * Returns a unique id.
	 * 
	 * @return
	 */
	public static String getUID() {
		return "trig_" + UUID.randomUUID().toString();
	}
	
	/**
	 * Converts bytes into megabytes.
	 * 
	 * @param l
	 * @return
	 */
	public static String byteToMB(long l) {
		return String.valueOf((Double.valueOf(l) / (1024 * 1024)));
	}
	
	public static String byteToGB(long l) {
		return String.valueOf((Double.valueOf(l) / (1024 * 1024 * 1024)));
	}
	
	public static String millisToHumanReadableDuration(long millis) {
		long value = millis / 1000; // millis to seconds
		long secondes = value % 60;
		value = value / 60;
		long minutes = value % 60;
		value = value / 60;
		long hours = value % 24;
		long days = value / 24;
		
		String time = "";
		if (days > 0)
			time += days + "d ";
		if (hours > 0)
			time += hours + "H ";
		if (minutes > 0)
			time += minutes + "mn ";
		
		time += secondes + "s";
		
		return time;
	}
	
}
