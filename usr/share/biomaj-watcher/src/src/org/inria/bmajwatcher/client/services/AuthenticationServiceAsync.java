package org.inria.bmajwatcher.client.services;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Asynchronous counterpart of <code>AuthenticationService</code> that is
 * used client side.
 * 
 * @author rsabas
 *
 */
public interface AuthenticationServiceAsync {

	public static final AuthenticationServiceAsync INSTANCE = GWT.create(AuthenticationService.class);
	
	void authenticate(String login, String passwd,
			AsyncCallback<Boolean> callback);

	void getUserAccess(AsyncCallback<String> callback);

	void logout(AsyncCallback<Void> callback);
}
