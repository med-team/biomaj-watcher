package org.inria.bmajwatcher.client.ui;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceLinkField;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class ErrorGridDataSource extends RestDataSource {

	public static final String BANK 	= "bank";
	public static final String SOURCE	= "source";
	public static final String MESSAGE	= "message";
	public static final String DATE		= "date";
	public static final String TYPE		= "type";
	public static final String LOG		= "log";
	
	private static DataSource instance = new ErrorGridDataSource();
	
	private ErrorGridDataSource() {
		setID("errorDatasource");
		
		DataSourceIntegerField pkField = new DataSourceIntegerField("iderror");
		pkField.setHidden(true);
		pkField.setPrimaryKey(true);
		
		
		DataSourceTextField bank = new DataSourceTextField(BANK);
		DataSourceTextField type = new DataSourceTextField(TYPE);
		DataSourceTextField message = new DataSourceTextField(MESSAGE);
		DataSourceLinkField log = new DataSourceLinkField(LOG);
		DataSourceTextField source = new DataSourceTextField(SOURCE);
		DataSourceTextField date = new DataSourceTextField(DATE);
		
		setFields(type, message, log, bank, source, date);
		
		setDataURL(GWT.getModuleName() + "/errorListingService");
	}
	
	private static void updateDataURL(String key) {
		Map<String, String> auth = new HashMap<String, String>();
		
		// Authenticate
		if (key != null) {
			auth.put("auth_key", key);	
		}
		
		instance.setDefaultParams(auth);
	}
	
	public static DataSource getInstance(String key) {
		updateDataURL(key);
		return instance;
	}
}
