package org.inria.bmajwatcher.client.ui.bank_editor;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.Image;

public class PropertyCell extends AbstractCell<String> {
	
	public static final Map<String, String> categories = new HashMap<String, String>();
	static {
		// key, title:color
		categories.put("download", "Download:#006D80");
		categories.put("init", "Initialization:#BDA44D");
		categories.put("ftpconfig", "Connection configuration:#78A419");
		categories.put("general", "Miscellaneous:#FC7F3C");
		categories.put("httpconfig", "HTTP configuration:#456B35");
		categories.put("deployment", "Deployment:#6E81BD");
		categories.put("makerelease", "Make release:#036635");
		categories.put("release", "Release:#CB60D2");
		categories.put("extract", "Extract:#ECA500");
		categories.put("directhttpconfig", "Direct HTTP configuration:#C96D63");
	}
	
	public static final String SEARCH_IN_NAME = "name";
	public static final String SEARCH_IN_DESC=  "description";
	public static final String SEARCH_IN_BOTH=  "both";
	
	public static final String SORT_ON_NAME =  "name_sort";
	public static final String SORT_ON_CAT =  "cat_sort";
	
	private Image image = new Image("images/edit-redo.png");

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			String value, SafeHtmlBuilder sb) {
		if (value == null)
			return;
		
		// value = propname:type
		String[] split = value.split(":");
		
		sb.appendHtmlConstant("<table>");

		
		String type = split[1];
		if (categories.get(split[1]) != null) {
			String[] s = categories.get(split[1]).split(":");
			type = "<span style=\"color:"+ s[1] + "\">" + s[0] + "</span>";
		}
		
		sb.appendHtmlConstant("<tr>");
		
		if (Boolean.valueOf(split[2])) {
			sb.appendHtmlConstant("<td rowspan=\"2\"><image src=\"" + image.getUrl() + "\" alt=\"inherited\" title=\"inherited\"/></td>");	
		}
		
		String color = "#000000";
		if (Boolean.valueOf(split[3])) {
			color = "#B72A41";
		}
		sb.appendHtmlConstant("<td><span style=\"color:" + color + "\"><b>" + split[0] + "</b></span></td></tr>");
		sb.appendHtmlConstant("<tr><td>" + type + "</td></tr>");
		
		sb.appendHtmlConstant("</table>");
	}
	
}
