package org.inria.bmajwatcher.server;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.inria.bmajwatcher.client.services.BankStatService;
import org.inria.bmajwatcher.server.data.BankRetriever;
import org.inria.bmajwatcher.server.data.StatsRetriever;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class BankStatServiceImpl extends RemoteServiceServlet implements BankStatService {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(BankStatServiceImpl.class);

	@Override
	public Map<String, Integer> banksStatus() {
		try {
			// Wait for lock to be acquired in bankRetriever.getBankList
			// if this method is called first
			Thread.sleep(500);
		} catch (InterruptedException e) {
			log.error(e);
		}
		Map<String, Integer> res = new HashMap<String, Integer>();
		
		res.put("error", BankRetriever.getInstance().getError());
		res.put("total", BankRetriever.getInstance().getTotal());
		res.put("updating", BankRetriever.getInstance().getUpdating());
		
		return res;
	}

	@Override
	public List<Map<String, String>> getBankStats(String bankName, int max) {
		return StatsRetriever.getBankStats(bankName, max);
	}

	@Override
	public List<Map<String, String>> getBanksInfo(String userKey) {
		return StatsRetriever.getBanksSize(userKey);
	}

}
