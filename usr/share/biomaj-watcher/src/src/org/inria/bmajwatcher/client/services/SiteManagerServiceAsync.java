package org.inria.bmajwatcher.client.services;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SiteManagerServiceAsync {

	public static final SiteManagerServiceAsync INSTANCE = GWT.create(SiteManagerService.class);
	
	void getSites(AsyncCallback<Map<String, String>> callback);

	void deleteSite(String url, AsyncCallback<Boolean> callback);

	void updateSite(String refUrl, String newUrl, String newDescription,
			AsyncCallback<Boolean> callback);

	void addSite(String url, String description, AsyncCallback<Boolean> callback);

}
