package org.inria.bmajwatcher.client.services;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BiomajLauncherServiceAsync {

	public static final BiomajLauncherServiceAsync INSTANCE = GWT.create(BiomajLauncherService.class);
	
	void startUpdate(String bankName, AsyncCallback<Boolean> callback);

	void startUpdateFromScratch(String bankName, AsyncCallback<Boolean> callback);

	void stopUpdate(String bankName, AsyncCallback<Boolean> callback);

	void startUpdateNew(String bankName, AsyncCallback<Boolean> callback);

	void rebuild(String bankName, AsyncCallback<Boolean> callback);

}
