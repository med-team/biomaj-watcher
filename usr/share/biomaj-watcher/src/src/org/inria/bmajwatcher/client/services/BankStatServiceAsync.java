package org.inria.bmajwatcher.client.services;

import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BankStatServiceAsync {

	public static final BankStatServiceAsync INSTANCE = GWT.create(BankStatService.class);
	
	void banksStatus(AsyncCallback<Map<String, Integer>> callback);

	void getBankStats(String bankName, int max,
			AsyncCallback<List<Map<String, String>>> callback);

	void getBanksInfo(String userKey, AsyncCallback<List<Map<String, String>>> callback);

}
