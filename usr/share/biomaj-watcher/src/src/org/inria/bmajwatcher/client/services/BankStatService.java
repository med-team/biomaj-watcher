package org.inria.bmajwatcher.client.services;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("bankStatService")
public interface BankStatService extends RemoteService {
	
	/**
	 * Returns global statistics about the banks managed by biomaj.
	 * - number of online banks
	 * - number of error banks
	 * - number of updating banks
	 * 
	 * @return
	 */
	public Map<String, Integer> banksStatus();
	
	/**
	 * Returns list of stats for each update of a given bank up
	 * to max updates.
	 * 
	 * @param bankName
	 * @param max if <= 0, return all updates
	 * @return
	 */
	public List<Map<String, String>> getBankStats(String bankName, int max);
	
	/**
	 * Returns info about all the online banks.
	 * These are :
	 * 	- size of the bank
	 * 	- name of the bank
	 * 	- server from where the bank was downloaded
	 * 	- type of the bank
	 * 
	 * @param userkey that identifies the user whose group is be considered
	 * @return
	 */
	public List<Map<String, String>> getBanksInfo(String userKey);
}
