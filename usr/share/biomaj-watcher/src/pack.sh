#!/bin/bash -e

svn export svn://scm.gforge.inria.fr/svn/biomaj/trunk/biomaj-watcher_deb biomaj_deb

# BmajWatcher
mkdir -p biomaj_deb/usr/share/biomaj-watcher
svn export svn://scm.gforge.inria.fr/svn/biomaj/trunk/biomaj_ext/BmajWatcher biomaj_deb/usr/share/biomaj-watcher/webapp

cp -R biomaj_deb/usr/share/biomaj-watcher/webapp/* biomaj_deb/usr/share/biomaj-watcher/src/
rm -rf biomaj_deb/usr/share/biomaj-watcher/webapp
rm -rf biomaj_deb/usr/share/biomaj-watcher/src/doc
rm -rf biomaj_deb/usr/share/biomaj-watcher/src/test
rm -f biomaj_deb/usr/share/biomaj-watcher/src/war/WEB-INF/lib/*
rm -rf  biomaj_deb/usr/share/biomaj-watcher/src/war/bmajwatcher

mv biomaj_deb/usr/share/biomaj-watcher/src/build.xml.debian  biomaj_deb/usr/share/biomaj-watcher/src/build.xml

mkdir -p biomaj_deb/usr/share/doc/biomaj-watcher
svn export svn://scm.gforge.inria.fr/svn/biomaj/trunk/biomaj_ext/BmajWatcher/doc/BW_userguide.pdf biomaj_deb/usr/share/doc/biomaj-watcher/BW_userguide.pdf
svn export svn://scm.gforge.inria.fr/svn/biomaj/trunk/biomaj_ext/BmajWatcher/doc/BW_doc.docx biomaj_deb/usr/share/doc/biomaj-watcher/BW_userguide.docx

svn export  http://gwt-dnd.googlecode.com/svn/tags/gwt-dnd-3.0.1-r1016/DragDrop/NOTICE biomaj_deb/usr/share/doc/biomaj-watcher/gwt-dnd_NOTICE
mkdir -p biomaj_deb/external_deps
svn export http://smartgwt.googlecode.com/svn/tags/2.4 biomaj_deb/external_deps/smartgwt
rm -rf biomaj_deb/external_deps/smartgwt/samples

mv biomaj_deb biomaj-watcher_$WATCHERVERSION


tar --exclude biomajwatcher_$WATCHERVERSION/debian -cvzf  biomaj-watcher_$WATCHERVERSION.orig.tar.gz biomaj-watcher_$WATCHERVERSION

rm -rf biomaj-watcher_$WATCHERVERSION

#dpkg-buildpackage
#cd ..
#lintian -i -I --show-overrides  biomaj_1.1.0-1_amd64.changes
